/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.spring.session.r2dbc;

import io.r2dbc.spi.Connection;
import io.r2dbc.spi.ConnectionFactory;
import io.r2dbc.spi.Result;
import io.r2dbc.spi.Statement;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.reactivestreams.Publisher;
import org.springframework.data.r2dbc.connectionfactory.R2dbcTransactionManager;
import org.springframework.data.r2dbc.core.DatabaseClient;
import org.springframework.data.r2dbc.core.DefaultReactiveDataAccessStrategy;
import org.springframework.data.r2dbc.dialect.PostgresDialect;
import org.springframework.lang.Nullable;
import org.springframework.session.MapSession;
import org.springframework.transaction.reactive.TransactionalOperator;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.Duration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

class R2dbcSessionRepositoryTest {

    @Mock
    Connection connection;

    private DatabaseClient databaseClient;

    private R2dbcSessionRepository sessionRepository;

    private DatabaseClient.Builder databaseClientBuilder;

    private TransactionalOperator transactionalOperator;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);

        ConnectionFactory connectionFactory = Mockito.mock(ConnectionFactory.class);

        when(connectionFactory.create()).thenReturn((Publisher) Mono.just(connection));
        when(connection.close()).thenReturn(Mono.empty());
        when(connection.beginTransaction()).thenReturn(Mono.empty());
        when(connection.rollbackTransaction()).thenReturn(Mono.empty());
        when(connection.commitTransaction()).thenReturn(Mono.empty());

        databaseClientBuilder = DatabaseClient.builder() //
                .connectionFactory(connectionFactory) //
                .dataAccessStrategy(new DefaultReactiveDataAccessStrategy(PostgresDialect.INSTANCE));

        this.databaseClient = databaseClientBuilder.build();
        this.transactionalOperator = TransactionalOperator.create(new R2dbcTransactionManager(connectionFactory));
        this.sessionRepository = new R2dbcSessionRepository(transactionalOperator, databaseClient);
    }

    @Test
    public void test_construct_null_r2dbc_operations() {
        assertThatIllegalArgumentException().isThrownBy(() -> new R2dbcSessionRepository(null, databaseClient))
                .withMessage("'transactionalOperator' must not be null");
    }

    @Test
    public void test_construct_null_db_client() {
        assertThatIllegalArgumentException().isThrownBy(() -> new R2dbcSessionRepository(transactionalOperator, null))
                .withMessage("'databaseClient' must not be null");
    }

    @Test
    public void test_set_null_table_name() {
        assertThatIllegalArgumentException().isThrownBy(() -> this.sessionRepository.setTableName(null))
                .withMessage("Table name must not be empty");
    }

    @Test
    public void test_set_empty_table_name() {
        assertThatIllegalArgumentException().isThrownBy(() -> this.sessionRepository.setTableName(" "))
                .withMessage("Table name must not be empty");
    }

    @Test
    public void test_set_create_session_query_null() {
        assertThatIllegalArgumentException().isThrownBy(() -> this.sessionRepository.setCreateSessionQuery(null))
                .withMessage("Query must not be empty");
    }

    @Test
    public void est_set_create_session_query_empty() {
        assertThatIllegalArgumentException().isThrownBy(() -> this.sessionRepository.setCreateSessionQuery(" "))
                .withMessage("Query must not be empty");
    }

    @Test
    public void test_set_create_session_attribute_query_null() {
        assertThatIllegalArgumentException().isThrownBy(() -> this.sessionRepository.setCreateSessionAttributeQuery(null))
                .withMessage("Query must not be empty");
    }

    @Test
    public void test_set_create_session_attribute_query_empty() {
        assertThatIllegalArgumentException().isThrownBy(() -> this.sessionRepository.setCreateSessionAttributeQuery(" "))
                .withMessage("Query must not be empty");
    }

    @Test
    public void test_set_get_session_query_null() {
        assertThatIllegalArgumentException().isThrownBy(() -> this.sessionRepository.setGetSessionQuery(null))
                .withMessage("Query must not be empty");
    }

    @Test
    public void test_set_get_session_query_empty() {
        assertThatIllegalArgumentException().isThrownBy(() -> this.sessionRepository.setGetSessionQuery(" "))
                .withMessage("Query must not be empty");
    }

    @Test
    public void test_set_update_session_query_null() {
        assertThatIllegalArgumentException().isThrownBy(() -> this.sessionRepository.setUpdateSessionQuery(null))
                .withMessage("Query must not be empty");
    }

    @Test
    public void test_set_update_session_query_empty() {
        assertThatIllegalArgumentException().isThrownBy(() -> this.sessionRepository.setUpdateSessionQuery(" "))
                .withMessage("Query must not be empty");
    }

    @Test
    public void test_set_update_session_attribute_query_null() {
        assertThatIllegalArgumentException().isThrownBy(() -> this.sessionRepository.setUpdateSessionAttributeQuery(null))
                .withMessage("Query must not be empty");
    }

    @Test
    public void test_update_session_attribute_query_empty() {
        assertThatIllegalArgumentException().isThrownBy(() -> this.sessionRepository.setUpdateSessionAttributeQuery(" "))
                .withMessage("Query must not be empty");
    }

    @Test
    public void test_set_delete_session_attribute_query_null() {
        assertThatIllegalArgumentException().isThrownBy(() -> this.sessionRepository.setDeleteSessionAttributeQuery(null))
                .withMessage("Query must not be empty");
    }

    @Test
    public void test_set_delete_session_attribute_query_empty() {
        assertThatIllegalArgumentException().isThrownBy(() -> this.sessionRepository.setDeleteSessionAttributeQuery(" "))
                .withMessage("Query must not be empty");
    }

    @Test
    public void test_set_delete_session_query_null() {
        assertThatIllegalArgumentException().isThrownBy(() -> this.sessionRepository.setDeleteSessionQuery(null))
                .withMessage("Query must not be empty");
    }

    @Test
    public void test_set_delete_session_query_empty() {
        assertThatIllegalArgumentException().isThrownBy(() -> this.sessionRepository.setDeleteSessionQuery(" "))
                .withMessage("Query must not be empty");
    }

    @Test
    public void test_set_delete_sessions_by_last_access_time_query_null() {
        assertThatIllegalArgumentException().isThrownBy(() -> this.sessionRepository.setDeleteSessionsByExpiryTimeQuery(null))
                .withMessage("Query must not be empty");
    }

    @Test
    public void test_set_delete_sessions_by_last_access_time_query_empty() {
        assertThatIllegalArgumentException().isThrownBy(() -> this.sessionRepository.setDeleteSessionsByExpiryTimeQuery(" "))
                .withMessage("Query must not be empty");
    }

    @Test
    public void test_set_conversation_service_null() {
        assertThatIllegalArgumentException().isThrownBy(() -> this.sessionRepository.setConversionService(null))
                .withMessage("conversionService must not be null");
    }

    @Test
    public void test_set_save_mode_null() {
        assertThatIllegalArgumentException().isThrownBy(() -> this.sessionRepository.setSaveMode(null))
                .withMessage("'saveMode' must not be null");
    }

    @Test
    public void test_create_session__default_max_inactive_interval() {
        StepVerifier.create(this.sessionRepository.createSession())
                .assertNext(session -> {
                    assertThat(session.isNew()).isTrue();
                    assertThat(session.getMaxInactiveInterval()).isEqualTo(new MapSession().getMaxInactiveInterval());
                }).verifyComplete();
    }

    @Test
    public void test_create_session_custom_max_inactive_interval() {
        int interval = 1;
        this.sessionRepository.setDefaultMaxInactiveInterval(interval);

        StepVerifier.create(this.sessionRepository.createSession())
                .assertNext(session -> {
                    assertThat(session.isNew()).isTrue();
                    assertThat(session.getMaxInactiveInterval()).isEqualTo(Duration.ofSeconds(interval));
                }).verifyComplete();
    }

    @Test
    public void test_get_attribute_names_and_remove() {
        R2dbcSessionRepository.R2dbcSession session = this.sessionRepository.createSession().block();
        session.setAttribute("attribute1", "value1");
        session.setAttribute("attribute2", "value2");

        for (String attributeName : session.getAttributeNames()) {
            session.removeAttribute(attributeName);
        }

        assertThat(session.getAttributeNames()).isEmpty();
    }

    @Test
    public void test_save_unchanged() {
        R2dbcSessionRepository.R2dbcSession session = this.sessionRepository.new R2dbcSession(new MapSession(), "primaryKey", false);

        this.sessionRepository.save(session);

        assertThat(session.isNew()).isFalse();
    }

    private Statement mockStatement() {
        return mockStatementFor(null, null);
    }

    private Statement mockStatement(Result result) {
        return mockStatementFor(null, result);
    }

    private Statement mockStatementFor(String sql) {
        return mockStatementFor(sql, null);
    }

    private Statement mockStatementFor(@Nullable String sql, @Nullable Result result) {

        Statement statement = mock(Statement.class);
        when(connection.createStatement(sql == null ? anyString() : eq(sql))).thenReturn(statement);
        when(statement.returnGeneratedValues(anyString())).thenReturn(statement);
        when(statement.returnGeneratedValues()).thenReturn(statement);

        doReturn(result == null ? Mono.empty() : Flux.just(result)).when(statement).execute();

        return statement;
        
    }
}
