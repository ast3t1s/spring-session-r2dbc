/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.spring.session.r2dbc.integrational;

import io.r2dbc.spi.ConnectionFactories;
import io.r2dbc.spi.ConnectionFactory;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.r2dbc.connectionfactory.init.ResourceDatabasePopulator;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.containers.PostgreSQLR2DBCDatabaseContainer;
import org.testcontainers.r2dbc.R2DBCDatabaseContainer;

@ExtendWith(SpringExtension.class)
@WebAppConfiguration
@ContextConfiguration
public class PostgreSql10R2dbcSessionRepositoryTests extends AbstractContainerR2dbcSessionRepositoryITests {

    @Configuration
    static class Config extends BaseContainerConfig {

        @Bean
        public ConnectionFactory connectionFactory(PostgreSQLContainer<?> container) {
            return ConnectionFactories.get(PostgreSQLR2DBCDatabaseContainer.getOptions(container));
        }

        @Bean(initMethod = "start", destroyMethod = "stop")
        public PostgreSQLContainer<?> databaseContainer() {
            return DatabaseContainers.postgreSql10();
        }

        @Bean(destroyMethod = "stop")
        public R2DBCDatabaseContainer r2DBCDatabaseContainer(PostgreSQLContainer<?> container) {
            return new PostgreSQLR2DBCDatabaseContainer(container);
        }

        @Bean
        public ResourceDatabasePopulator databasePopulator() {
            return new ResourceDatabasePopulator(new ClassPathResource("com/spring/session/r2dbc/schema-postgresql.sql"));
        }

    }
}
