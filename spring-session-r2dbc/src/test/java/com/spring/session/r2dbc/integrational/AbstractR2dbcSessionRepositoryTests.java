/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.spring.session.r2dbc.integrational;

import com.spring.session.r2dbc.R2dbcSessionRepository;
import com.spring.session.r2dbc.config.EnableR2dbcWebSession;
import io.r2dbc.spi.ConnectionFactory;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.r2dbc.connectionfactory.R2dbcTransactionManager;
import org.springframework.session.MapSession;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.ReactiveTransactionManager;
import reactor.test.StepVerifier;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.function.Consumer;
import java.util.function.Supplier;

public abstract class AbstractR2dbcSessionRepositoryTests {

    @Autowired
    private R2dbcSessionRepository sessionRepository;

    @Test
    public void test_save_when_no_attributes_then_can_be_found() {
        StepVerifier.create(this.sessionRepository.createSession())
                .assertNext(session -> {
                    Assertions.assertThat(session).isNotNull();
                    Assertions.assertThat(session.isChanged()).isFalse();
                    Assertions.assertThat(session.getDelta()).isEmpty();
                }).verifyComplete();

        StepVerifier.create(this.sessionRepository.createSession()
                .flatMap(session -> sessionRepository.save(session)))
                .expectNextCount(0)
                .verifyComplete();
    }

    @Test
    public void test_save() {
        R2dbcSessionRepository.R2dbcSession session = this.sessionRepository.createSession().block();
        String expectedAttributeName = "a";
        String expectedAttributeValue = "b";
        session.setAttribute(expectedAttributeName, expectedAttributeValue);

        StepVerifier.create(this.sessionRepository.save(session))
                .expectNextCount(0)
                .verifyComplete();

        StepVerifier.create(this.sessionRepository.findById(session.getId()))
                .assertNext(saved -> {
                    Assertions.assertThat(saved.getId()).isEqualTo(session.getId());
                    Assertions.assertThat(saved.isChanged()).isFalse();
                    Assertions.assertThat(saved.getDelta()).isEmpty();
                    Assertions.assertThat(saved.getAttributeNames()).isEqualTo(session.getAttributeNames());
                    Assertions.assertThat(saved.getAttribute(expectedAttributeName).toString()).isEqualTo(session.getAttribute(expectedAttributeName));
                }).verifyComplete();

        StepVerifier.create(this.sessionRepository.deleteById(session.getId()))
                .verifyComplete();

        StepVerifier.create(this.sessionRepository.findById(session.getId()))
                .expectNextCount(0)
                .verifyComplete();
    }

    @Test
    public void test_put_all_on_single_attr_does_not_remove_old() {
        R2dbcSessionRepository.R2dbcSession toSave = this.sessionRepository.createSession().block();
        toSave.setAttribute("a", "b");

        StepVerifier.create(this.sessionRepository.save(toSave))
                .verifyComplete();

        StepVerifier.create(this.sessionRepository.findById(toSave.getId()))
                .expectNextCount(1)
                .verifyComplete();

        toSave.setAttribute("1", "2");

        StepVerifier.create(this.sessionRepository.save(toSave))
                .verifyComplete();

        StepVerifier.create(this.sessionRepository.findById(toSave.getId()))
                .assertNext(session -> {
                    Assertions.assertThat(session.isChanged()).isFalse();
                    Assertions.assertThat(session.getDelta()).isEmpty();
                    Assertions.assertThat(session.getAttributeNames().size()).isEqualTo(2);
                    Assertions.assertThat(session.<String>getAttribute("a")).isEqualTo("b");
                    Assertions.assertThat(session.<String>getAttribute("1")).isEqualTo("2");
                }).verifyComplete();

        StepVerifier.create(this.sessionRepository.deleteById(toSave.getId()))
                .verifyComplete();
    }

    @Test
    public void test_update_last_access_time() {
        R2dbcSessionRepository.R2dbcSession toSave = this.sessionRepository.createSession().block();
        toSave.setLastAccessedTime(Instant.now().minusSeconds(MapSession.DEFAULT_MAX_INACTIVE_INTERVAL_SECONDS + 1));

        StepVerifier.create(this.sessionRepository.save(toSave))
                .verifyComplete();

        Instant lastAccessedTime = Instant.now();
        toSave.setLastAccessedTime(lastAccessedTime);

        StepVerifier.create(this.sessionRepository.save(toSave))
                .verifyComplete();

        StepVerifier.create(this.sessionRepository.findById(toSave.getId()))
                .assertNext(session -> {
                    Assertions.assertThat(session).isNotNull();
                    Assertions.assertThat(session.isChanged()).isFalse();
                    Assertions.assertThat(session.getDelta()).isEmpty();
                    Assertions.assertThat(session.isExpired()).isFalse();
                    Assertions.assertThat(session.getLastAccessedTime().truncatedTo(ChronoUnit.MINUTES))
                            .isEqualTo(lastAccessedTime.truncatedTo(ChronoUnit.MINUTES));
                }).verifyComplete();
    }

    @Test
    public void test_cleanup_inactive_sessions_using_repository_defined_interval() {
        R2dbcSessionRepository.R2dbcSession session = this.sessionRepository.createSession().block();

        StepVerifier.create(this.sessionRepository.save(session))
                .verifyComplete();

        StepVerifier.create(this.sessionRepository.findById(session.getId()))
                .expectNextCount(1)
                .verifyComplete();

        StepVerifier.create(this.sessionRepository.clearExpiredSessions())
                .verifyComplete();

        StepVerifier.create(this.sessionRepository.findById(session.getId()))
                .expectNextCount(1)
                .verifyComplete();

        Instant now = Instant.now();

        session.setLastAccessedTime(now.minus(10, ChronoUnit.MINUTES));

        StepVerifier.create(this.sessionRepository.save(session))
                .verifyComplete();

        StepVerifier.create(this.sessionRepository.clearExpiredSessions())
                .verifyComplete();

        StepVerifier.create(this.sessionRepository.findById(session.getId()))
                .expectNextCount(1)
                .verifyComplete();

        session.setLastAccessedTime(now.minus(30, ChronoUnit.MINUTES));

        StepVerifier.create(this.sessionRepository.save(session))
                .verifyComplete();

        StepVerifier.create(this.sessionRepository.clearExpiredSessions())
                .verifyComplete();

        StepVerifier.create(this.sessionRepository.findById(session.getId()))
                .expectNextCount(0)
                .verifyComplete();
    }

    @Test // gh-580
    public void test_cleanup_inactive_sessions_using_session_defined_interval() {
        R2dbcSessionRepository.R2dbcSession session = this.sessionRepository.createSession().block();
        session.setMaxInactiveInterval(Duration.ofMinutes(45));

        StepVerifier.create(this.sessionRepository.save(session))
                .verifyComplete();

        StepVerifier.create(this.sessionRepository.findById(session.getId()))
                .expectNextCount(1)
                .verifyComplete();

        StepVerifier.create(this.sessionRepository.clearExpiredSessions())
                .verifyComplete();

        Instant now = Instant.now();

        session.setLastAccessedTime(now.minus(40, ChronoUnit.MINUTES));

        StepVerifier.create(this.sessionRepository.save(session))
                .verifyComplete();

        StepVerifier.create(this.sessionRepository.clearExpiredSessions())
                .verifyComplete();

        StepVerifier.create(this.sessionRepository.findById(session.getId()))
                .expectNextCount(1)
                .verifyComplete();

        session.setLastAccessedTime(now.minus(50, ChronoUnit.MINUTES));

        StepVerifier.create(this.sessionRepository.save(session))
                .verifyComplete();

        StepVerifier.create(this.sessionRepository.clearExpiredSessions())
                .verifyComplete();

        StepVerifier.create(this.sessionRepository.findById(session.getId()))
                .expectNextCount(0)
                .verifyComplete();
    }

    @Test
    public void test_change_session_id_when_only_change_id() {
        String attrName = "changeSessionId";
        String attrValue = "changeSessionId-value";
        R2dbcSessionRepository.R2dbcSession toSave = this.sessionRepository.createSession().block();
        toSave.setAttribute(attrName, attrValue);

        StepVerifier.create(this.sessionRepository.save(toSave))
                .verifyComplete();

        StepVerifier.create(this.sessionRepository.findById(toSave.getId()))
                .assertNext(session ->
                        Assertions.assertThat(session.<String>getAttribute(attrName)).isEqualTo(attrValue))
                .verifyComplete();

        String originalFindById = toSave.getId();
        String changeSessionId = toSave.changeSessionId();

        StepVerifier.create(this.sessionRepository.save(toSave))
                .verifyComplete();

        StepVerifier.create(this.sessionRepository.findById(originalFindById))
                .expectNextCount(0)
                .verifyComplete();

        StepVerifier.create(this.sessionRepository.findById(changeSessionId))
                .assertNext(new Consumer<R2dbcSessionRepository.R2dbcSession>() {
                    @Override
                    public void accept(R2dbcSessionRepository.R2dbcSession session) {
                        Assertions.assertThat(session.isChanged()).isFalse();
                        Assertions.assertThat(session.getDelta()).isEmpty();
                        Assertions.assertThat(session.<String>getAttribute(attrName)).isEqualTo(attrValue);
                    }
                }).verifyComplete();

    }

    @Test
    public void test_change_session_id_when_change_twice() {
        R2dbcSessionRepository.R2dbcSession toSave = this.sessionRepository.createSession().block();

        StepVerifier.create(this.sessionRepository.save(toSave))
                .verifyComplete();

        String originalId = toSave.getId();
        String changeId1 = toSave.changeSessionId();
        String changeId2 = toSave.changeSessionId();

        StepVerifier.create(this.sessionRepository.save(toSave))
                .verifyComplete();

        StepVerifier.create(this.sessionRepository.findById(originalId))
                .expectNextCount(0)
                .verifyComplete();

        StepVerifier.create(this.sessionRepository.findById(changeId1))
                .expectNextCount(0)
                .verifyComplete();

        StepVerifier.create(this.sessionRepository.findById(changeId2))
                .expectNextCount(1)
                .verifyComplete();
    }

    @Test
    public void test_change_session_id_when_set_attribute_on_changed_session() {
        String attrName = "changeSessionId";
        String attrValue = "changeSessionId-value";

        R2dbcSessionRepository.R2dbcSession toSave = this.sessionRepository.createSession().block();

        StepVerifier.create(this.sessionRepository.save(toSave))
                .verifyComplete();

        R2dbcSessionRepository.R2dbcSession findById = this.sessionRepository.findById(toSave.getId()).block();

        findById.setAttribute(attrName, attrValue);

        String originalFindById = findById.getId();
        String changeSessionId = findById.changeSessionId();

        StepVerifier.create(this.sessionRepository.save(findById))
                .verifyComplete();

        StepVerifier.create(this.sessionRepository.findById(originalFindById))
                .expectNextCount(0)
                .verifyComplete();

        StepVerifier.create(this.sessionRepository.findById(changeSessionId))
                .assertNext(session -> {
                    Assertions.assertThat(session.isChanged()).isFalse();
                    Assertions.assertThat(session.getDelta()).isEmpty();
                    Assertions.assertThat(session.<String>getAttribute(attrName)).isEqualTo(attrValue);
                }).verifyComplete();
    }

    @Test
    public void test_change_session_id_when_has_not_saved() {
        R2dbcSessionRepository.R2dbcSession toSave = this.sessionRepository.createSession().block();
        String originalId = toSave.getId();
        toSave.changeSessionId();

        StepVerifier.create(this.sessionRepository.save(toSave))
                .verifyComplete();

        StepVerifier.create(this.sessionRepository.findById(toSave.getId()))
                .expectNextCount(1)
                .verifyComplete();

        StepVerifier.create(this.sessionRepository.findById(originalId))
                .expectNextCount(0)
                .verifyComplete();
    }

    @Test // gh-1070
    public void test_save_updated_add_and_modify_attribute() {
        R2dbcSessionRepository.R2dbcSession session = this.sessionRepository.createSession().block();
        this.sessionRepository.save(session).block();

        session = this.sessionRepository.findById(session.getId()).block();
        session.setAttribute("testName", "testValue1");
        session.setAttribute("testName", "testValue2");

        StepVerifier.create(this.sessionRepository.save(session))
                .verifyComplete();

        StepVerifier.create(this.sessionRepository.findById(session.getId()))
                .assertNext(new Consumer<R2dbcSessionRepository.R2dbcSession>() {
                    @Override
                    public void accept(R2dbcSessionRepository.R2dbcSession session) {
                        Assertions.assertThat(session.<String>getAttribute("testName")).isEqualTo("testValue2");

                    }
                }).verifyComplete();
    }

    @Test // gh-1070
    public void test_save_updated_modify_and_remove_attribute() {
        R2dbcSessionRepository.R2dbcSession session = this.sessionRepository.createSession().block();
        session.setAttribute("testName", "testValue1");

        StepVerifier.create(this.sessionRepository.save(session))
                .verifyComplete();

        session = this.sessionRepository.findById(session.getId()).block();
        session.setAttribute("testName", "testValue2");
        session.removeAttribute("testName");

        StepVerifier.create(this.sessionRepository.save(session))
                .verifyComplete();

        StepVerifier.create(this.sessionRepository.findById(session.getId()))
                .assertNext(session1 -> Assertions.assertThat(session1.<String>getAttribute("testName")).isNull()).verifyComplete();
    }

    @Test // gh-1070
    public void test_save_updated_remove_and_add_attribute() {
        R2dbcSessionRepository.R2dbcSession session = this.sessionRepository.createSession().block();
        session.setAttribute("testName", "testValue1");

        StepVerifier.create(this.sessionRepository.save(session))
                .verifyComplete();

        session = this.sessionRepository.findById(session.getId()).block();
        session.removeAttribute("testName");
        session.setAttribute("testName", "testValue2");

        StepVerifier.create(this.sessionRepository.save(session))
                .verifyComplete();

        StepVerifier.create(this.sessionRepository.findById(session.getId()))
                .assertNext(session1 -> Assertions.assertThat(session1.<String>getAttribute("testName")).isEqualTo("testValue2"))
                .verifyComplete();
    }

    @Test // gh-1031
    public void test_save_deleted() {
        R2dbcSessionRepository.R2dbcSession session = this.sessionRepository.createSession().block();

        StepVerifier.create(this.sessionRepository.save(session))
                .verifyComplete();

        StepVerifier.create(this.sessionRepository.findById(session.getId()))
                .expectNextCount(1)
                .verifyComplete();

        StepVerifier.create(this.sessionRepository.deleteById(session.getId()))
                .verifyComplete();

        session.setLastAccessedTime(Instant.now());

        StepVerifier.create(this.sessionRepository.save(session))
                .verifyComplete();

        StepVerifier.create(this.sessionRepository.findById(session.getId()))
                .expectNextCount(0)
                .verifyComplete();
    }

    @Test // gh-1031
    public void test_save_deleted_add_attribute() {
        R2dbcSessionRepository.R2dbcSession session = this.sessionRepository.createSession().block();

        StepVerifier.create(this.sessionRepository.save(session))
                .verifyComplete();

        StepVerifier.create(this.sessionRepository.findById(session.getId()))
                .expectNextCount(1)
                .verifyComplete();

        StepVerifier.create(this.sessionRepository.deleteById(session.getId()))
                .verifyComplete();

        session.setLastAccessedTime(Instant.now());
        session.setAttribute("testName", "testValue1");

        StepVerifier.create(this.sessionRepository.save(session))
                .verifyComplete();

        StepVerifier.create(this.sessionRepository.findById(session.getId()))
                .expectNextCount(0)
                .verifyComplete();
    }

    @Test // gh-1133
    public void test_session_from_store_resolve_attributes_lazily() {
        R2dbcSessionRepository.R2dbcSession session = this.sessionRepository.createSession().block();
        session.setAttribute("attribute1", "value1");
        session.setAttribute("attribute2", "value2");

        StepVerifier.create(this.sessionRepository.save(session))
                .verifyComplete();

        StepVerifier.create(this.sessionRepository.findById(session.getId()))
                .assertNext(session1 -> {
                    MapSession delegate = (MapSession) ReflectionTestUtils.getField(session1, "delegate");

                    Supplier attribute1 = delegate.getAttribute("attribute1");
                    Assertions.assertThat(ReflectionTestUtils.getField(attribute1, "value")).isNull();
                    Assertions.assertThat((String) session1.getAttribute("attribute1")).isEqualTo("value1");
                    Assertions.assertThat(ReflectionTestUtils.getField(attribute1, "value")).isEqualTo("value1");
                    Supplier attribute2 = delegate.getAttribute("attribute2");
                    Assertions.assertThat(ReflectionTestUtils.getField(attribute2, "value")).isNull();
                    Assertions.assertThat((String) session1.getAttribute("attribute2")).isEqualTo("value2");
                    Assertions.assertThat(ReflectionTestUtils.getField(attribute2, "value")).isEqualTo("value2");
                }).verifyComplete();
    }

    @Test // gh-1203
    public void test_save_with_large_attribute() {
        final String attributeName = "largeAttribute";
        final int arraySize = 4096;

        R2dbcSessionRepository.R2dbcSession session = this.sessionRepository.createSession().block();
        session.setAttribute(attributeName, new byte[arraySize]);

        StepVerifier.create(this.sessionRepository.save(session))
                .expectNextCount(0)
                .verifyComplete();

        StepVerifier.create(this.sessionRepository.findById(session.getId()))
                .assertNext(session1 -> {
                    Assertions.assertThat(session1).isNotNull();
                    Assertions.assertThat((byte[]) session1.getAttribute(attributeName)).hasSize(arraySize);
                }).verifyComplete();
    }


    @EnableR2dbcWebSession
    public static class BaseTestConfig {

        @Bean
        public ReactiveTransactionManager reactiveTransactionManager(ConnectionFactory connectionFactory) {
            return new R2dbcTransactionManager(connectionFactory);
        }
    }
} 
