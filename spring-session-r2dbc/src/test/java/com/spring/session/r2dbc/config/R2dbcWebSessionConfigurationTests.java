/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.spring.session.r2dbc.config;

import com.spring.session.r2dbc.R2dbcSessionRepository;
import com.spring.session.r2dbc.config.annotation.SpringSessionConnectionFactory;
import com.spring.session.r2dbc.config.annotation.SpringSessionDatabaseClient;
import io.r2dbc.spi.ConnectionFactory;
import io.r2dbc.spi.test.MockConnectionFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.annotation.Order;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.r2dbc.core.DatabaseClient;
import org.springframework.data.r2dbc.core.DefaultReactiveDataAccessStrategy;
import org.springframework.data.r2dbc.dialect.PostgresDialect;
import org.springframework.mock.env.MockEnvironment;
import org.springframework.session.config.ReactiveSessionRepositoryCustomizer;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.ReactiveTransactionManager;
import org.springframework.transaction.reactive.TransactionalOperator;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.mock;

public class R2dbcWebSessionConfigurationTests {

    private static final int MAX_INACTIVE_INTERVAL_IN_SECONDS = 600;

    private static final String TABLE_NAME = "TEST_SESSION";

    private AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();

    @BeforeEach
    public void closeContext() {
        if (this.context != null) {
            context.close();
        }
    }

    @Test
    public void test_no_connection_factory_config() {
        assertThatExceptionOfType(BeanCreationException.class)
                .isThrownBy(() -> registerAndRefresh(NoConnectionFactoryConfiguration.class))
                .withMessageContaining("expected at least 1 bean which qualifies as autowire candidate");
    }

    @Test
    public void test_default_configuration() {
        registerAndRefresh(ConnectionFactoryConfiguration.class, DefaultConfiguration.class);

        R2dbcSessionRepository sessionRepository = this.context.getBean(R2dbcSessionRepository.class);
        assertThat(sessionRepository).isNotNull();
    }

    @Test
    public void test_custom_table_name_annotation() {
        registerAndRefresh(ConnectionFactoryConfiguration.class, CustomTableNameAnnotationConfiguration.class);

        R2dbcSessionRepository repository = this.context.getBean(R2dbcSessionRepository.class);
        assertThat(repository).isNotNull();
        assertThat(ReflectionTestUtils.getField(repository, "tableName")).isEqualTo(TABLE_NAME);
    }

    @Test
    public void test_custom_table_name_setter() {
        registerAndRefresh(ConnectionFactoryConfiguration.class, CustomTableNameSetterConfiguration.class);

        R2dbcSessionRepository repository = this.context.getBean(R2dbcSessionRepository.class);
        assertThat(repository).isNotNull();
        assertThat(ReflectionTestUtils.getField(repository, "tableName")).isEqualTo(TABLE_NAME);
    }

    @Test
    public void test_custom_max_inactive_interval_in_seonds_annotation() {
        registerAndRefresh(ConnectionFactoryConfiguration.class,
                CustomMaxInactiveIntervalInSecondsAnnotationConfiguration.class);

        R2dbcSessionRepository repository = this.context.getBean(R2dbcSessionRepository.class);
        assertThat(repository).isNotNull();
        assertThat(ReflectionTestUtils.getField(repository, "defaultMaxInactiveInterval"))
                .isEqualTo(MAX_INACTIVE_INTERVAL_IN_SECONDS);
    }

    @Test
    public void test_quliaified_connection_factory_configuration() {
        registerAndRefresh(ConnectionFactoryQualifiedConnectionFactory.class, QualifiedConnectionFactoryConfiguration.class);

        R2dbcSessionRepository repository = this.context.getBean(R2dbcSessionRepository.class);
        ConnectionFactory connectionFactory = this.context.getBean("qualifiedConnectionFactory", ConnectionFactory.class);
        assertThat(repository).isNotNull();
        assertThat(connectionFactory).isNotNull();
        DatabaseClient databaseClient = (DatabaseClient) ReflectionTestUtils.getField(repository, "databaseClient");
        assertThat(databaseClient).isNotNull();
        assertThat(ReflectionTestUtils.getField(databaseClient, "connector")).isEqualTo(connectionFactory);
    }

    @Test
    public void test_primary_connection_factory_configuration() {
        registerAndRefresh(ConnectionFactoryConfiguration.class, PrimaryConnectionFactoryConfiguration.class);

        R2dbcSessionRepository repository = this.context.getBean(R2dbcSessionRepository.class);
        ConnectionFactory connectionFactory = this.context.getBean("primaryConnectionFactory", ConnectionFactory.class);
        assertThat(repository).isNotNull();
        assertThat(connectionFactory).isNotNull();
        DatabaseClient databaseClient = (DatabaseClient) ReflectionTestUtils.getField(repository, "databaseClient");
        assertThat(databaseClient).isNotNull();
        assertThat(ReflectionTestUtils.getField(databaseClient, "connector")).isEqualTo(connectionFactory);
    }

    @Test
    public void test_qualified_and_primary_connection_factory_configuration() {
        registerAndRefresh(ConnectionFactoryQualifiedConnectionFactory.class, QualifiedAndPrimaryConnectionFactoryConfiguration.class);

        R2dbcSessionRepository repository = this.context.getBean(R2dbcSessionRepository.class);
        ConnectionFactory connectionFactory = this.context.getBean("qualifiedConnectionFactory", ConnectionFactory.class);
        assertThat(repository).isNotNull();
        assertThat(connectionFactory).isNotNull();
        DatabaseClient databaseClient = (DatabaseClient) ReflectionTestUtils.getField(repository, "databaseClient");
        assertThat(databaseClient).isNotNull();
        assertThat(ReflectionTestUtils.getField(databaseClient, "connector")).isEqualTo(connectionFactory);
    }

    @Test
    public void test_multiple_connection_factories_configuration() {
        assertThatExceptionOfType(BeanCreationException.class)
                .isThrownBy(
                        () -> registerAndRefresh(ConnectionFactoryConfiguration.class, MultipleConnectionFactoryConfiguration.class))
                .withMessageContaining("expected single matching bean but found 2");
    }


    @Test
    public void test_custom_transaction_operations_configuration() {
        registerAndRefresh(ConnectionFactoryConfiguration.class, CustomTransactionOperationsConfiguration.class);

        R2dbcSessionRepository repository = this.context.getBean(R2dbcSessionRepository.class);
        TransactionalOperator transactionOperations = this.context.getBean(TransactionalOperator.class);
        assertThat(repository).isNotNull();
        assertThat(transactionOperations).isNotNull();
        assertThat(repository).hasFieldOrPropertyWithValue("transactionalOperator", transactionOperations);
    }

    @Test
    public void test_custom_conversation_service_configuration() {
        registerAndRefresh(ConnectionFactoryConfiguration.class, CustomConversionServiceConfiguration.class);

        R2dbcSessionRepository repository = this.context.getBean(R2dbcSessionRepository.class);
        ConversionService conversionService = this.context.getBean("springSessionConversionService",
                ConversionService.class);
        assertThat(repository).isNotNull();
        assertThat(conversionService).isNotNull();
        Object repositoryConversionService = ReflectionTestUtils.getField(repository, "conversionService");
        assertThat(repositoryConversionService).isEqualTo(conversionService);
    }

    @Test
    public void test_resolve_table_name_by_property_placeholder() {
        this.context
                .setEnvironment(new MockEnvironment().withProperty("session.jdbc.tableName", "custom_session_table"));
        registerAndRefresh(ConnectionFactoryConfiguration.class, CustomJdbcHttpSessionConfiguration.class);
        R2dbcSessionConfiguration configuration = this.context.getBean(R2dbcSessionConfiguration.class);
        assertThat(ReflectionTestUtils.getField(configuration, "tableName")).isEqualTo("custom_session_table");
    }

    @Test
    public void test_session_repository_customizer() {
        registerAndRefresh(ConnectionFactoryConfiguration.class, SessionRepositoryCustomizerConfiguration.class);
        R2dbcSessionRepository sessionRepository = this.context.getBean(R2dbcSessionRepository.class);
        assertThat(sessionRepository).hasFieldOrPropertyWithValue("defaultMaxInactiveInterval",
                MAX_INACTIVE_INTERVAL_IN_SECONDS);
    }

    @Test
    public void test_qualified_database_client() {
        registerAndRefresh(QualifiedDatabaseClientConfiguration.class, DefaultConfiguration.class);
        R2dbcSessionRepository sessionRepository = this.context.getBean(R2dbcSessionRepository.class);

        DatabaseClient databaseClient = this.context.getBean("databaseClient", DatabaseClient.class);
        assertThat(sessionRepository).isNotNull();
        assertThat(databaseClient).isNotNull();
        DatabaseClient jdbcOperations = (DatabaseClient) ReflectionTestUtils.getField(sessionRepository, "databaseClient");
        assertThat(jdbcOperations).isNotNull();
        assertThat(jdbcOperations).isEqualTo(databaseClient);
    }

    private void registerAndRefresh(Class<?>... annotatedClasses) {
        this.context.register(annotatedClasses);
        this.context.refresh();
    }

    @EnableR2dbcWebSession
    static class NoConnectionFactoryConfiguration {

    }

    @Configuration
    static class ConnectionFactoryConfiguration {

        @Bean
        public DatabaseClient databaseClient(ConnectionFactory connectionFactory) {
            return DatabaseClient.builder() //
                    .connectionFactory(connectionFactory) //
                    .dataAccessStrategy(new DefaultReactiveDataAccessStrategy(PostgresDialect.INSTANCE))
                    .build();
        }

        @Bean
        public ConnectionFactory defaultConnectionFactory() {
            return MockConnectionFactory.empty();
        }

        @Bean
        public ReactiveTransactionManager transactionManager() {
            return mock(ReactiveTransactionManager.class);
        }

    }

    @Configuration
    static class QualifiedDatabaseClientConfiguration {

        @Bean
        public DatabaseClient noQualifiedClient(ConnectionFactory connectionFactory) {
            return DatabaseClient.builder() //
                    .connectionFactory(connectionFactory) //
                    .dataAccessStrategy(new DefaultReactiveDataAccessStrategy(PostgresDialect.INSTANCE))
                    .build();
        }

        @Bean
        @SpringSessionDatabaseClient
        public DatabaseClient databaseClient(ConnectionFactory connectionFactory) {
            return DatabaseClient.builder() //
                    .connectionFactory(connectionFactory) //
                    .dataAccessStrategy(new DefaultReactiveDataAccessStrategy(PostgresDialect.INSTANCE))
                    .build();
        }

        @Bean
        public ConnectionFactory defaultConnectionFactory() {
            return MockConnectionFactory.empty();
        }

        @Bean
        public ReactiveTransactionManager transactionManager() {
            return mock(ReactiveTransactionManager.class);
        }
    }

    @Configuration
    static class ConnectionFactoryQualifiedConnectionFactory {

        @Bean
        public DatabaseClient databaseClient(@SpringSessionConnectionFactory ConnectionFactory connectionFactory) {
            return DatabaseClient.builder() //
                    .connectionFactory(connectionFactory) //
                    .dataAccessStrategy(new DefaultReactiveDataAccessStrategy(PostgresDialect.INSTANCE))
                    .build();
        }

        @Bean
        public ConnectionFactory defaultConnectionFactory() {
            return MockConnectionFactory.empty();
        }

        @Bean
        public ReactiveTransactionManager transactionManager() {
            return mock(ReactiveTransactionManager.class);
        }
    }

    @EnableR2dbcWebSession(tableName = TABLE_NAME)
    static class CustomTableNameAnnotationConfiguration {

    }

    @Configuration
    static class CustomTableNameSetterConfiguration extends R2dbcSessionConfiguration {

        CustomTableNameSetterConfiguration() {
            setTableName(TABLE_NAME);
        }

    }

    @EnableR2dbcWebSession(maxInactiveIntervalInSeconds = MAX_INACTIVE_INTERVAL_IN_SECONDS)
    static class CustomMaxInactiveIntervalInSecondsAnnotationConfiguration {

    }

    @EnableR2dbcWebSession
    static class QualifiedConnectionFactoryConfiguration {

        @Bean
        @SpringSessionConnectionFactory
        public ConnectionFactory qualifiedConnectionFactory() {
            return MockConnectionFactory.empty();
        }

    }

    @EnableR2dbcWebSession
    static class PrimaryConnectionFactoryConfiguration {

        @Bean
        @Primary
        public ConnectionFactory primaryConnectionFactory() {
            return MockConnectionFactory.empty();
        }

    }

    @EnableR2dbcWebSession
    static class QualifiedAndPrimaryConnectionFactoryConfiguration {

        @Bean
        @SpringSessionConnectionFactory
        public ConnectionFactory qualifiedConnectionFactory() {
            return MockConnectionFactory.empty();
        }

        @Bean
        @Primary
        ConnectionFactory primaryConnectionFactory() {
            return MockConnectionFactory.empty();
        }

    }

    @EnableR2dbcWebSession
    static class MultipleConnectionFactoryConfiguration {

        @Bean
        ConnectionFactory secondaryConnectionFactory() {
            return MockConnectionFactory.empty();
        }

    }

    @EnableR2dbcWebSession
    static class CustomTransactionOperationsConfiguration {

        @Bean
        TransactionalOperator springSessionTransactionOperations(ReactiveTransactionManager reactiveTransactionManager) {
            return TransactionalOperator.create(reactiveTransactionManager);
        }

    }

    @EnableR2dbcWebSession
    static class CustomConversionServiceConfiguration {

        @Bean
        ConversionService springSessionConversionService() {
            return mock(ConversionService.class);
        }

    }

    @EnableR2dbcWebSession(tableName = "${session.jdbc.tableName}")
    static class CustomJdbcHttpSessionConfiguration {

        @Bean
        PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
            return new PropertySourcesPlaceholderConfigurer();
        }

    }

    @EnableR2dbcWebSession
    static class SessionRepositoryCustomizerConfiguration {

        @Bean
        @Order(0)
        ReactiveSessionRepositoryCustomizer<R2dbcSessionRepository> sessionRepositoryCustomizerOne() {
            return (sessionRepository) -> sessionRepository.setDefaultMaxInactiveInterval(0);
        }

        @Bean
        @Order(1)
        ReactiveSessionRepositoryCustomizer<R2dbcSessionRepository> sessionRepositoryCustomizerTwo() {
            return (sessionRepository) -> sessionRepository
                    .setDefaultMaxInactiveInterval(MAX_INACTIVE_INTERVAL_IN_SECONDS);
        }

    }

    @EnableR2dbcWebSession
    static class DefaultConfiguration {

    }
} 
