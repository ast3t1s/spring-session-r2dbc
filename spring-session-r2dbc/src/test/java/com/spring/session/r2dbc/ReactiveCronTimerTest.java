/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.spring.session.r2dbc;

import com.spring.session.r2dbc.scheduler.ReactiveScheduler;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.function.Supplier;

import static org.mockito.Mockito.*;

public class ReactiveCronTimerTest {

    private final Supplier<Mono<Void>> checkFn = mock(Supplier.class, (i) -> Mono.empty());

    private final ReactiveScheduler reactiveScheduler = new ReactiveScheduler("test", this.checkFn, Duration.ofMillis(10));

    @AfterEach
    public void tearDown() {
        this.reactiveScheduler.stop();
    }

    @Test
    public void test_should_check_after_being_started() throws InterruptedException {
        this.reactiveScheduler.start();
        Thread.sleep(1000);
        verify(this.checkFn, atLeastOnce()).get();
    }

    @Test
    public void test_should_not_check_when_stopped() throws InterruptedException {
        this.reactiveScheduler.stop();
        Thread.sleep(1000);
        verify(this.checkFn, never()).get();
    }

    @Test
    public void test_should_check_after_error() throws InterruptedException {
        when(this.checkFn.get()).thenReturn(Mono.error(new RuntimeException("Test"))).thenReturn(Mono.empty());

        this.reactiveScheduler.start();
        Thread.sleep(1000);
        verify(this.checkFn, atLeast(2)).get();
    }
} 
