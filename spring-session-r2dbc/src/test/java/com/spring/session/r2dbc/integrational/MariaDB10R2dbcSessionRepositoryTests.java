/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.spring.session.r2dbc.integrational;

import io.r2dbc.spi.ConnectionFactories;
import io.r2dbc.spi.ConnectionFactory;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.r2dbc.connectionfactory.init.ResourceDatabasePopulator;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.testcontainers.containers.MariaDBContainer;
import org.testcontainers.containers.MariaDBR2DBCDatabaseContainer;
import org.testcontainers.r2dbc.R2DBCDatabaseContainer;

@ExtendWith(SpringExtension.class)
@WebAppConfiguration
@ContextConfiguration
public class MariaDB10R2dbcSessionRepositoryTests extends AbstractContainerR2dbcSessionRepositoryITests {

    public static class MariaDBContainerConfig extends BaseContainerConfig {

        @Bean
        public ConnectionFactory connectionFactory(MariaDBContainer<?> mariaDBContainer) {
            return ConnectionFactories.get(MariaDBR2DBCDatabaseContainer.getOptions(mariaDBContainer));
        }

        @Bean(initMethod = "start",destroyMethod = "stop")
        public MariaDBContainer<?> mariaDBContainer() {
            return DatabaseContainers.mariaDb10();
        }

        @Bean(destroyMethod = "stop")
        public R2DBCDatabaseContainer mySqlR2dbcContainer(MariaDBContainer<?> mariaDBContainer) {
            return new MariaDBR2DBCDatabaseContainer(mariaDBContainer);
        }

        @Bean
        public ResourceDatabasePopulator databasePopulator() {
            return new ResourceDatabasePopulator(new ClassPathResource("com/spring/session/r2dbc/schema-mysql.sql"));
        }
    }
}
