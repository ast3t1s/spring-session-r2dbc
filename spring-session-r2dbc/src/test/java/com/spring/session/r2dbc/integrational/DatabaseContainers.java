/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.spring.session.r2dbc.integrational;

import org.testcontainers.containers.MSSQLServerContainer;
import org.testcontainers.containers.MariaDBContainer;
import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.containers.PostgreSQLContainer;

public class DatabaseContainers {

    private DatabaseContainers() {
    }

    static MariaDBContainer mariaDb5() {
        return new MariaDb5Container();
    }

    static MariaDBContainer mariaDb10() {
        return new MariaDb10Container();
    }

    static MySQLContainer mySql5() {
        return new MySql5Container();
    }

    static MySQLContainer mySql8() {
        return new MySql8Container();
    }

    static PostgreSQLContainer postgreSql9() {
        return new PostgreSql9Container();
    }

    static PostgreSQLContainer postgreSql10() {
        return new PostgreSql10Container();
    }

    static PostgreSQLContainer postgreSql11() {
        return new PostgreSql11Container();
    }

    static MSSQLServerContainer sqlServer2017() {
        return new SqlServer2017Container();
    }

    private static class MariaDb5Container extends MariaDBContainer<MariaDb5Container> {

        MariaDb5Container() {
            super("mariadb:5.5.64");
        }

        @Override
        protected void configure() {
            super.configure();
            setCommand("mysqld", "--character-set-server=utf8mb4", "--collation-server=utf8mb4_unicode_ci",
                    "--innodb_large_prefix", "--innodb_file_format=barracuda", "--innodb-file-per-table");
        }

    }

    private static class MariaDb10Container extends MariaDBContainer<MariaDb10Container> {

        MariaDb10Container() {
            super("mariadb:10.4.8");
        }

        @Override
        protected void configure() {
            super.configure();
            setCommand("mysqld", "--character-set-server=utf8mb4", "--collation-server=utf8mb4_unicode_ci");
        }

    }

    private static class MySql5Container extends MySQLContainer<MySql5Container> {

        MySql5Container() {
            super("mysql:5.7.27");
        }

        @Override
        protected void configure() {
            super.configure();
            setCommand("mysqld", "--character-set-server=utf8mb4", "--collation-server=utf8mb4_unicode_ci");
        }

        @Override
        public String getDriverClassName() {
            return "com.mysql.cj.jdbc.Driver";
        }

    }

    private static class MySql8Container extends MySQLContainer<MySql8Container> {

        MySql8Container() {
            super("mysql:8.0.17");
        }

        @Override
        protected void configure() {
            super.configure();
            setCommand("mysqld", "--default-authentication-plugin=mysql_native_password");
        }

        @Override
        public String getDriverClassName() {
            return "com.mysql.cj.jdbc.Driver";
        }

    }


    private static class PostgreSql9Container extends PostgreSQLContainer<PostgreSql9Container> {

        PostgreSql9Container() {
            super("postgres:9.6.15");
        }

    }

    private static class PostgreSql10Container extends PostgreSQLContainer<PostgreSql10Container> {

        PostgreSql10Container() {
            super("postgres:10.10");
        }

    }

    private static class PostgreSql11Container extends PostgreSQLContainer<PostgreSql11Container> {

        PostgreSql11Container() {
            super("postgres:11.5");
        }

    }

    private static class SqlServer2017Container extends MSSQLServerContainer<SqlServer2017Container> {

        SqlServer2017Container() {
            super("mcr.microsoft.com/mssql/server:2017-CU16");
            withConnectTimeoutSeconds(120);
        }

    }
} 
