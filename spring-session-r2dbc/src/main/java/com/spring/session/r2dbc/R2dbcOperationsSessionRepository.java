/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.spring.session.r2dbc;

import org.springframework.data.r2dbc.core.DatabaseClient;
import org.springframework.session.ReactiveSessionRepository;
import org.springframework.transaction.ReactiveTransactionManager;
import org.springframework.transaction.reactive.TransactionalOperator;

/**
 * This {@link ReactiveSessionRepository} implementation is kept in order to support
 * migration to {@link R2dbcSessionRepository} in a backwards compatible manner.
 *
 * @author Vsevolod Bondarenko
 * @deprecated since 2.2.0 in favor of {@link R2dbcSessionRepository}
 */
@Deprecated
public class R2dbcOperationsSessionRepository extends R2dbcSessionRepository {

    /**
     * Create a new {@link R2dbcOperationsSessionRepository} instance which uses the
     * provided {@link DatabaseClient} and {@link TransactionalOperator} to manage
     * sessions.
     * @param databaseClient the {@link DatabaseClient} to use
     * @param transactionalOperator the {@link TransactionalOperator} to use
     * @see R2dbcSessionRepository#R2dbcSessionRepository(TransactionalOperator,
     * DatabaseClient)
     */
    public R2dbcOperationsSessionRepository(TransactionalOperator transactionalOperator, DatabaseClient databaseClient) {
        super(transactionalOperator, databaseClient);
    }

    /**
     * Create a new {@link R2dbcOperationsSessionRepository} instance which uses the provided
     * {@link DatabaseClient} to manage sessions.
     * <p>
     * @param databaseClient the {@link DatabaseClient} to use
     * @param transactionManager the {@link ReactiveTransactionManager} to use
     * @deprecated since 2.2.0 in favor of
     * {@link R2dbcSessionRepository#R2dbcSessionRepository(TransactionalOperator, DatabaseClient)}
     */
    @Deprecated
    public R2dbcOperationsSessionRepository(ReactiveTransactionManager transactionManager, DatabaseClient databaseClient) {
        super(TransactionalOperator.create(transactionManager), databaseClient);
    }
}
