/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.spring.session.r2dbc.config;

import com.spring.session.r2dbc.R2dbcSessionRepository;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.session.MapSession;
import org.springframework.session.SaveMode;
import org.springframework.session.web.http.SessionRepositoryFilter;

import java.lang.annotation.*;

/**
 * Add this annotation to an {@code @Configuration} class to expose the
 * {@link SessionRepositoryFilter} as a bean named {@code springSessionRepositoryFilter}
 * and backed by a relational database. In order to leverage the annotation, a single
 * {@link io.r2dbc.spi.ConnectionFactory} must be provided. For example:
 *
 * <pre class="code">
 * &#064;Configuration
 * &#064;EnableR2dbcWebSession
 * public class R2dbcWebSessionConfig extends AbstractR2dbcConfiguration {
 *
 *     &#064;Bean
 *     &#064;Override
 *     public ConnectionFactory connectionFactory() {
 *         return new PostgresqlConnectionFactory(PostgresqlConnectionConfiguration.builder()
 *                 .host("localhost")
 *                 .port(5433)
 *                 .username("postgres")
 *                 .password("password")
 *                 .database("postgres")
 *                 .build()
 *         );
 *     }
 *
 *     &#064;Bean
 *     public ReactiveTransactionManager transactionManager(ConnectionFactory connectionFactory) {
 *         return new R2dbcTransactionManager(connectionFactory);
 *     }
 *
 * }
 * </pre>
 *
 * More advanced configurations can extend {@link R2dbcSessionConfiguration} instead.
 *
 * For additional information on how to configure data access related concerns, please
 * refer to the <a href=
 * "https://docs.spring.io/spring/docs/current/spring-framework-reference/html/spring-data-tier.html">
 * Spring Framework Reference Documentation</a>.
 *
 * @author Vsevolod Bondarenko
 * @see org.springframework.session.config.annotation.web.server.EnableSpringWebSession
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
@Import(R2dbcSessionConfiguration.class)
@Configuration(proxyBeanMethods = false)
public @interface EnableR2dbcWebSession {

    /**
     * The session timeout in seconds. By default, it is set to 1800 seconds (30 minutes).
     * This should be a non-negative integer.
     * @return the seconds a session can be inactive before expiring
     */
    int maxInactiveIntervalInSeconds() default MapSession.DEFAULT_MAX_INACTIVE_INTERVAL_SECONDS;

    /**
     * The name of database table used by Spring Session to store sessions.
     * @return the database table name
     */
    String tableName() default R2dbcSessionRepository.DEFAULT_TABLE_NAME;

    /**
     * The cron expression for expired session cleanup job. By default runs every minute (60 seconds).
     * @return the session cleanup cron time in seconds
     */
    int cleanCronInSeconds() default R2dbcSessionRepository.CRON_TIMER_DEFAULT;

    /**
     * Save mode for the session. The default is {@link SaveMode#ON_SET_ATTRIBUTE}, which
     * only saves changes made to session.
     * @return the save mode
     */
    SaveMode saveMode() default SaveMode.ON_SET_ATTRIBUTE;;
}
