/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.spring.session.r2dbc.scheduler;

import com.spring.session.r2dbc.R2dbcSessionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Mono;

import java.time.Duration;

public class SessionCleaner {

    private static final Logger log = LoggerFactory.getLogger(SessionCleaner.class);

    private final R2dbcSessionRepository repository;

    private final ReactiveScheduler intervalCheck;

    public SessionCleaner(R2dbcSessionRepository repository) {
        this.repository = repository;
        this.intervalCheck = new ReactiveScheduler("session", this::cleanExpiredSessions);
    }

    public void start() {
        this.intervalCheck.start();
    }

    public void stop() {
        this.intervalCheck.stop();
    }

    protected Mono<Void> cleanExpiredSessions() {
        return this.repository.clearExpiredSessions().onErrorResume((e) -> {
            log.warn("Unexpected error while cleaning expired sessions", e);
            return Mono.empty();
        });
    }

    public void setInterval(Duration updateInterval) {
        this.intervalCheck.setInterval(updateInterval);
    }

}
