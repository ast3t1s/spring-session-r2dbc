/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.spring.session.r2dbc.scheduler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.lang.Nullable;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;
import reactor.util.retry.Retry;

import java.time.Duration;
import java.util.function.Supplier;
import java.util.logging.Level;

public class ReactiveScheduler {

    private static final Logger log = LoggerFactory.getLogger(ReactiveScheduler.class);

    private final String name;

    private final Supplier<Mono<Void>> task;

    private Duration interval;

    @Nullable
    private Disposable subscription;

    @Nullable
    private Scheduler scheduler;

    public ReactiveScheduler(String name, Supplier<Mono<Void>> task) {
        this(name, task, Duration.ofSeconds(10));
    }

    public ReactiveScheduler(String name, Supplier<Mono<Void>> task, Duration interval) {
        this.name = name;
        this.task = task;
        this.interval = interval;
    }

    public void start() {
        this.scheduler = Schedulers.newSingle(this.name + "-check");
        this.subscription = Flux.interval(this.interval)
                .doOnSubscribe((s) -> log.info("Scheduled {}-check every {}", this.name, this.interval))
                .log(log.getName(), Level.FINEST).subscribeOn(this.scheduler).concatMap((i) -> task.get())
                .retryWhen(Retry.max(Long.MAX_VALUE).doAfterRetry((ctx) -> log.warn("Unexpected error", ctx.failure())))
                .subscribe();
    }

    public void stop() {
        if (this.subscription != null) {
            this.subscription.dispose();
            this.subscription = null;
        }
        if (this.scheduler != null) {
            this.scheduler.dispose();
            this.scheduler = null;
        }
    }

    public void setInterval(Duration interval) {
        this.interval = interval;
    }
} 
