/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.spring.session.r2dbc.config;

import com.spring.session.r2dbc.R2dbcSessionRepository;
import com.spring.session.r2dbc.config.annotation.SpringSessionConnectionFactory;
import com.spring.session.r2dbc.config.annotation.SpringSessionDatabaseClient;
import com.spring.session.r2dbc.scheduler.SessionCleaner;
import io.r2dbc.spi.ConnectionFactory;
import org.springframework.beans.factory.BeanClassLoaderAware;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.EmbeddedValueResolverAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportAware;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.support.GenericConversionService;
import org.springframework.core.serializer.support.DeserializingConverter;
import org.springframework.core.serializer.support.SerializingConverter;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.data.r2dbc.core.DatabaseClient;
import org.springframework.session.MapSession;
import org.springframework.session.SaveMode;
import org.springframework.session.config.ReactiveSessionRepositoryCustomizer;
import org.springframework.session.config.annotation.web.server.SpringWebSessionConfiguration;
import org.springframework.transaction.ReactiveTransactionManager;
import org.springframework.transaction.reactive.TransactionalOperator;
import org.springframework.util.StringUtils;
import org.springframework.util.StringValueResolver;
import org.springframework.web.server.session.WebSessionManager;

import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Spring {@code @Configuration} class used to configure and initialize a R2dbc based
 * {@code WebSession} provider implementation in Spring Session.
 * <p>
 * Exposes the {@link WebSessionManager} as a bean named
 * {@code  webSessionManager}. In order to use this a single {@link ConnectionFactory}
 * must be exposed as a Bean.
 *
 * @author Vsevolod Bondarenko
 * @see EnableR2dbcWebSession
 */
@Configuration(proxyBeanMethods = false)
public class R2dbcSessionConfiguration extends SpringWebSessionConfiguration implements BeanClassLoaderAware, EmbeddedValueResolverAware, ImportAware {

    private Integer maxInactiveIntervalInSeconds = MapSession.DEFAULT_MAX_INACTIVE_INTERVAL_SECONDS;

    private SaveMode saveMode = SaveMode.ON_SET_ATTRIBUTE;

    private String tableName = R2dbcSessionRepository.DEFAULT_TABLE_NAME;

    private Integer cleanupCronInSeconds = R2dbcSessionRepository.CRON_TIMER_DEFAULT;

    private ApplicationEventPublisher applicationEventPublisher;

    private ClassLoader classLoader;

    private StringValueResolver embeddedValueResolver;

    private ConversionService springSessionConversionService;

    private ConversionService conversionService;

    private DatabaseClient databaseClient;

    private ConnectionFactory connectionFactory;

    private ReactiveTransactionManager reactiveTransactionManager;

    private TransactionalOperator transactionalOperator;

    private List<ReactiveSessionRepositoryCustomizer<R2dbcSessionRepository>> sessionRepositoryCustomizers;

    @Bean
    public R2dbcSessionRepository sessionRepository() {
        if (databaseClient == null) {
            this.databaseClient = DatabaseClient.create(this.connectionFactory);
        }
        if (this.transactionalOperator == null) {
            this.transactionalOperator = TransactionalOperator.create(this.reactiveTransactionManager);
        }
        R2dbcSessionRepository sessionRepository = new R2dbcSessionRepository(this.transactionalOperator, databaseClient);
        if (StringUtils.hasText(this.tableName)) {
            sessionRepository.setTableName(this.tableName);
        }
        sessionRepository.setDefaultMaxInactiveInterval(this.maxInactiveIntervalInSeconds);
        sessionRepository.setSaveMode(this.saveMode);
        if (this.applicationEventPublisher != null) {
            sessionRepository.setApplicationEventPublisher(this.applicationEventPublisher);
        }
        if (this.springSessionConversionService != null) {
            sessionRepository.setConversionService(this.springSessionConversionService);
        } else if (this.conversionService != null) {
            sessionRepository.setConversionService(this.conversionService);
        } else {
            sessionRepository.setConversionService(createConversionServiceWithBeanClassLoader(this.classLoader));
        }
        this.sessionRepositoryCustomizers.forEach(sessionRepositoryCustomizer -> sessionRepositoryCustomizer.customize(sessionRepository));
        return sessionRepository;
    }

    @Bean(initMethod = "start", destroyMethod = "stop")
    public SessionCleaner sessionCleanupTimer(R2dbcSessionRepository sessionRepository) {
        SessionCleaner sessionCleaner = new SessionCleaner(sessionRepository);
        sessionCleaner.setInterval(Duration.ofSeconds(this.cleanupCronInSeconds));
        return sessionCleaner;
    }

    @Override
    public void setBeanClassLoader(ClassLoader classLoader) {
        this.classLoader = classLoader;
    }

    @Override
    public void setEmbeddedValueResolver(StringValueResolver stringValueResolver) {
        this.embeddedValueResolver = stringValueResolver;
    }

    @Override
    public void setImportMetadata(AnnotationMetadata annotationMetadata) {
        Map<String, Object> attributeMap = annotationMetadata.getAnnotationAttributes(EnableR2dbcWebSession.class.getName());
        AnnotationAttributes attributes = AnnotationAttributes.fromMap(attributeMap);
        this.maxInactiveIntervalInSeconds = attributes.getNumber("maxInactiveIntervalInSeconds");
        String tableNameValue = attributes.getString("tableName");
        if (StringUtils.hasText(tableNameValue)) {
            this.tableName = this.embeddedValueResolver.resolveStringValue(tableNameValue);
        }
        this.cleanupCronInSeconds = attributes.getNumber("cleanCronInSeconds");
        this.saveMode = attributes.getEnum("saveMode");
    }

    public void setMaxInactiveIntervalInSeconds(Integer maxInactiveIntervalInSeconds) {
        this.maxInactiveIntervalInSeconds = maxInactiveIntervalInSeconds;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public void setCleanupCronInSeconds(Integer cleanupCronInSeconds) {
        this.cleanupCronInSeconds = cleanupCronInSeconds;
    }

    public void setSaveMode(SaveMode saveMode) {
        this.saveMode = saveMode;
    }

    @Autowired(required = false)
    @Qualifier("springSessionConversionService")
    public void setSpringSessionConversionService(ConversionService conversionService) {
        this.springSessionConversionService = conversionService;
    }

    @Autowired(required = false)
    @Qualifier("conversionService")
    public void setConversionService(ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Autowired(required = false)
    public void setDatabaseClient(@SpringSessionDatabaseClient ObjectProvider<DatabaseClient> springSessionDatabaseClient,
                                  ObjectProvider<DatabaseClient> databaseClient) {
        DatabaseClient clientToUse = springSessionDatabaseClient.getIfAvailable();
        if (clientToUse == null) {
            clientToUse = databaseClient.getIfAvailable();
        }
        this.databaseClient = clientToUse;
    }

    @Autowired
    public void setConnectionFactory(@SpringSessionConnectionFactory ObjectProvider<ConnectionFactory> springSessionConnectionFactory,
                                     ObjectProvider<ConnectionFactory> connectionFactory) {
        ConnectionFactory connectionFactoryToUse = springSessionConnectionFactory.getIfAvailable();
        if (connectionFactoryToUse == null) {
            connectionFactoryToUse = connectionFactory.getObject();
        }
        this.connectionFactory = connectionFactoryToUse;
    }

    @Autowired
    public void setReactiveTransactionManager(ReactiveTransactionManager transactionManager) {
        this.reactiveTransactionManager = transactionManager;
    }

    @Autowired(required = false)
    public void setTransactionalOperator(TransactionalOperator transactionalOperator) {
        this.transactionalOperator = transactionalOperator;
    }

    @Autowired(required = false)
    public void setApplicationEventPublisher(ApplicationEventPublisher eventPublisher) {
        this.applicationEventPublisher = eventPublisher;
    }

    @Autowired(required = false)
    public void setSessionRepositoryCustomizers(ObjectProvider<ReactiveSessionRepositoryCustomizer<R2dbcSessionRepository>> sessionRepositoryCustomizers) {
        this.sessionRepositoryCustomizers = sessionRepositoryCustomizers.orderedStream().collect(Collectors.toList());
    }

    private static GenericConversionService createConversionServiceWithBeanClassLoader(ClassLoader classLoader) {
        GenericConversionService conversionService = new GenericConversionService();
        conversionService.addConverter(Object.class, byte[].class, new SerializingConverter());
        conversionService.addConverter(byte[].class, Object.class, new DeserializingConverter(classLoader));
        return conversionService;
    }

}
