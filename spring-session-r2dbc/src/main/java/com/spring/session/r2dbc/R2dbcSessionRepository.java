/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.spring.session.r2dbc;

import io.r2dbc.spi.Blob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.support.GenericConversionService;
import org.springframework.core.serializer.support.DeserializingConverter;
import org.springframework.core.serializer.support.SerializingConverter;
import org.springframework.data.r2dbc.core.DatabaseClient;
import org.springframework.session.MapSession;
import org.springframework.session.ReactiveSessionRepository;
import org.springframework.session.SaveMode;
import org.springframework.session.Session;
import org.springframework.session.events.SessionCreatedEvent;
import org.springframework.transaction.reactive.TransactionalOperator;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.nio.ByteBuffer;
import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static com.spring.session.r2dbc.FuncUtils.lazily;
import static com.spring.session.r2dbc.FuncUtils.value;

/**
 * A {@link org.springframework.session.ReactiveSessionRepository} implementation that uses
 * Spring's {@link DatabaseClient} to store sessions in a relational database. This
 * implementation does not support publishing of session events.
 * <p>
 * An example of how to create a new instance can be seen below:
 *
 * <pre class="code">
 * DatabaseClient client = DatabaseClient.create(this.connectionFactory);
 *
 * // ... configure client ...
 *
 * TransactionalOperator transactionalOperator = TransactionalOperator.create(this.reactiveTransactionalManager;
 *
 * // ... configure transactionTemplate ...
 *
 * R2dbcSessionRepository sessionRepository =
 *         new R2dbcSessionRepository(transactionalOperator, client);
 * </pre>
 *
 * For additional information on how to create and configure {@code DatabaseClient} and
 * {@code TransactionalOperator}, refer to the <a href=
 * "https://docs.spring.io/spring/docs/current/spring-framework-reference/html/spring-data-tier.html">
 * Spring Framework Reference Documentation</a>.
 * <p>
 * By default, this implementation uses <code>SPRING_SESSION</code> and
 * <code>SPRING_SESSION_ATTRIBUTES</code> tables to store sessions. Note that the table
 * name can be customized using the {@link #setTableName(String)} method. In that case the
 * table used to store attributes will be named using the provided table name, suffixed
 * with <code>_ATTRIBUTES</code>.
 *
 * Depending on your database, the table definition can be described as below:
 *
 * <pre class="code">
 * CREATE TABLE SPRING_SESSION (
 *   PRIMARY_ID CHAR(36) NOT NULL,
 *   SESSION_ID CHAR(36) NOT NULL,
 *   CREATION_TIME BIGINT NOT NULL,
 *   LAST_ACCESS_TIME BIGINT NOT NULL,
 *   MAX_INACTIVE_INTERVAL INT NOT NULL,
 *   EXPIRY_TIME BIGINT NOT NULL,
 *   CONSTRAINT SPRING_SESSION_PK PRIMARY KEY (PRIMARY_ID)
 * );
 *
 * CREATE UNIQUE INDEX SPRING_SESSION_IX1 ON SPRING_SESSION (SESSION_ID);
 * CREATE INDEX SPRING_SESSION_IX2 ON SPRING_SESSION (EXPIRY_TIME);
 *
 * CREATE TABLE SPRING_SESSION_ATTRIBUTES (
 *  SESSION_PRIMARY_ID CHAR(36) NOT NULL,
 *  ATTRIBUTE_NAME VARCHAR(200) NOT NULL,
 *  ATTRIBUTE_BYTES BYTEA NOT NULL,
 *  CONSTRAINT SPRING_SESSION_ATTRIBUTES_PK PRIMARY KEY (SESSION_PRIMARY_ID, ATTRIBUTE_NAME),
 *  CONSTRAINT SPRING_SESSION_ATTRIBUTES_FK FOREIGN KEY (SESSION_PRIMARY_ID) REFERENCES SPRING_SESSION(PRIMARY_ID) ON DELETE CASCADE
 * );
 *
 * CREATE INDEX SPRING_SESSION_ATTRIBUTES_IX1 ON SPRING_SESSION_ATTRIBUTES (SESSION_PRIMARY_ID);
 * </pre>
 *
 * Due to the differences between the various database vendors, especially when it comes
 * to storing binary data, make sure to use SQL script specific to your database. Scripts
 * for most major database vendors are packaged as
 * <code>com/spring/session/r2dbc/schema-*.sql</code>, where <code>*</code> is the
 * target database type.
 *
 * @author Vsevolod Bondarenko
 */
public class R2dbcSessionRepository implements ReactiveSessionRepository<R2dbcSessionRepository.R2dbcSession>, ApplicationEventPublisherAware {

    private static final Logger log = LoggerFactory.getLogger(R2dbcSessionRepository.class);

    public static final String DEFAULT_TABLE_NAME = "SPRING_SESSION";

    public static final int CRON_TIMER_DEFAULT = 60;

    // @formatter:off
    private static final String CREATE_SESSION_ATTRIBUTE_QUERY = "INSERT INTO %TABLE_NAME%_ATTRIBUTES(SESSION_PRIMARY_ID, ATTRIBUTE_NAME, ATTRIBUTE_BYTES) "
            + "SELECT PRIMARY_ID, :attribute_name, :attribute_bytes "
            + "FROM %TABLE_NAME% "
            + "WHERE SESSION_ID = :session_id";
    // @formatter:on

    // @formatter:off
    public static final String CREATE_SESSION_QUERY = "INSERT INTO %TABLE_NAME%(PRIMARY_ID, SESSION_ID, CREATION_TIME, LAST_ACCESS_TIME, MAX_INACTIVE_INTERVAL, EXPIRY_TIME) "
            + "VALUES (:primary_id, :session_id, :creation_time, :last_access_time, :max_inactive_interval, :expiry_time)";
    // @formatter:on

    // @formatter:off
    public static final String GET_SESSION_QUERY = "SELECT S.PRIMARY_ID, S.SESSION_ID, S.CREATION_TIME, S.LAST_ACCESS_TIME, S.MAX_INACTIVE_INTERVAL, SA.ATTRIBUTE_NAME, SA.ATTRIBUTE_BYTES "
            + "FROM %TABLE_NAME% S "
            + "LEFT OUTER JOIN %TABLE_NAME%_ATTRIBUTES SA ON S.PRIMARY_ID = SA.SESSION_PRIMARY_ID "
            + "WHERE S.SESSION_ID = :session_id";
    // @formatter:on

    // @formatter:off
    private static final String UPDATE_SESSION_ATTRIBUTE_QUERY = "UPDATE %TABLE_NAME%_ATTRIBUTES SET ATTRIBUTE_BYTES = :attribute_bytes "
            + "WHERE SESSION_PRIMARY_ID = :session_primary_id "
            + "AND ATTRIBUTE_NAME = :attribute_name";
    // @formatter:on

    // @formatter:off
    private static final String UPDATE_SESSION_QUERY = "UPDATE %TABLE_NAME% SET SESSION_ID = :session_id, LAST_ACCESS_TIME = :last_access_time, MAX_INACTIVE_INTERVAL = :max_inactive_interval, " +
            "EXPIRY_TIME = :expiry_time WHERE PRIMARY_ID = :primary_id";
    // @formatter:on

    // @formatter:off
    private static final String DELETE_SESSION_ATTRIBUTE_QUERY = "DELETE FROM %TABLE_NAME%_ATTRIBUTES "
            + "WHERE SESSION_PRIMARY_ID = :session_primary_id "
            + "AND ATTRIBUTE_NAME = :attribute_name";
    // @formatter:on

    // @formatter:off
    private static final String DELETE_SESSION_QUERY = "DELETE FROM %TABLE_NAME% WHERE SESSION_ID = :session_id";
    // @formatter:on

    // @formatter:off
    private static final String DELETE_SESSIONS_BY_EXPIRY_TIME_QUERY = "DELETE FROM %TABLE_NAME% "
            + "WHERE EXPIRY_TIME < :expiry_time";
    // @formatter:on

    private ApplicationEventPublisher eventPublisher;

    private ConversionService conversionService = createDefaultConversionService();

    private String tableName = DEFAULT_TABLE_NAME;

    private Integer defaultMaxInactiveInterval;

    private SaveMode saveMode = SaveMode.ON_SET_ATTRIBUTE;

    private final TransactionalOperator transactionalOperator;

    private final DatabaseClient databaseClient;

    private String createSessionQuery;

    private String createSessionAttributeQuery;

    private String getSessionQuery;

    private String updateSessionQuery;

    private String updateSessionAttributeQuery;

    private String deleteSessionAttributeQuery;

    private String deleteSessionQuery;

    private String deleteSessionsByExpiryTimeQuery;

    public R2dbcSessionRepository(TransactionalOperator transactionalOperator, DatabaseClient databaseClient) {
        Assert.notNull(transactionalOperator, "'transactionalOperator' must not be null");
        Assert.notNull(databaseClient, "'databaseClient' must not be null");
        this.transactionalOperator = transactionalOperator;
        this.databaseClient = databaseClient;
        prepareQueries();
    }

    public void setSaveMode(final SaveMode saveMode) {
        Assert.notNull(saveMode, "'saveMode' must not be null");
        this.saveMode = saveMode;
    }

    /**
     * Set the name of database table used to store sessions.
     * @param tableName the database table name
     */
    public void setTableName(final String tableName) {
        Assert.hasText(tableName, "Table name must not be empty");
        this.tableName = tableName.trim();
    }

    /**
     * Set the custom SQL query used to create the session.
     * @param createSessionQuery the SQL query string
     */
    public void setCreateSessionQuery(String createSessionQuery) {
        Assert.hasText(createSessionQuery, "Query must not be empty");
        this.createSessionQuery = createSessionQuery;
    }

    /**
     * Set the custom SQL query used to create the session attribute.
     * @param createSessionAttributeQuery the SQL query string
     */
    public void setCreateSessionAttributeQuery(String createSessionAttributeQuery) {
        Assert.hasText(createSessionAttributeQuery, "Query must not be empty");
        this.createSessionAttributeQuery = createSessionAttributeQuery;
    }

    /**
     * Set the custom SQL query used to retrieve the session.
     * @param getSessionQuery the SQL query string
     */
    public void setGetSessionQuery(String getSessionQuery) {
        Assert.hasText(getSessionQuery, "Query must not be empty");
        this.getSessionQuery = getSessionQuery;
    }

    /**
     * Set the custom SQL query used to update the session.
     * @param updateSessionQuery the SQL query string
     */
    public void setUpdateSessionQuery(String updateSessionQuery) {
        Assert.hasText(updateSessionQuery, "Query must not be empty");
        this.updateSessionQuery = updateSessionQuery;
    }

    /**
     * Set the custom SQL query used to update the session attribute.
     * @param updateSessionAttributeQuery the SQL query string
     */
    public void setUpdateSessionAttributeQuery(String updateSessionAttributeQuery) {
        Assert.hasText(updateSessionAttributeQuery, "Query must not be empty");
        this.updateSessionAttributeQuery = updateSessionAttributeQuery;
    }

    /**
     * Set the custom SQL query used to delete the session attribute.
     * @param deleteSessionAttributeQuery the SQL query string
     */
    public void setDeleteSessionAttributeQuery(String deleteSessionAttributeQuery) {
        Assert.hasText(deleteSessionAttributeQuery, "Query must not be empty");
        this.deleteSessionAttributeQuery = deleteSessionAttributeQuery;
    }

    /**
     * Set the custom SQL query used to delete the session.
     * @param deleteSessionQuery the SQL query string
     */
    public void setDeleteSessionQuery(String deleteSessionQuery) {
        Assert.hasText(deleteSessionQuery, "Query must not be empty");
        this.deleteSessionQuery = deleteSessionQuery;
    }

    /**
     * Set the custom SQL query used to delete the sessions by last access time.
     * @param deleteSessionsByExpiryTimeQuery the SQL query string
     */
    public void setDeleteSessionsByExpiryTimeQuery(String deleteSessionsByExpiryTimeQuery) {
        Assert.hasText(deleteSessionsByExpiryTimeQuery, "Query must not be empty");
        this.deleteSessionsByExpiryTimeQuery = deleteSessionsByExpiryTimeQuery;
    }

    /**
     * Sets the {@link ConversionService} to use.
     * @param conversionService the converter to set
     */
    public void setConversionService(ConversionService conversionService) {
        Assert.notNull(conversionService, "conversionService must not be null");
        this.conversionService = conversionService;
    }

    /**
     * Set the maximum inactive interval in seconds between requests before newly created
     * sessions will be invalidated. A negative time indicates that the session will never
     * timeout. The default is 1800 (30 minutes).
     * @param defaultMaxInactiveInterval the maximum inactive interval in seconds
     */
    public void setDefaultMaxInactiveInterval(Integer defaultMaxInactiveInterval) {
        this.defaultMaxInactiveInterval = defaultMaxInactiveInterval;
    }

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher eventPublisher) {
        Assert.notNull(eventPublisher, "'eventPublisher' must not be null");
        this.eventPublisher = eventPublisher;
    }

    @Override
    public Mono<R2dbcSession> createSession() {
        return Mono.defer(() -> {
            MapSession delegate = new MapSession();
            if (this.defaultMaxInactiveInterval != null) {
                delegate.setMaxInactiveInterval(Duration.ofSeconds(this.defaultMaxInactiveInterval));
            }
            R2dbcSession r2dbcSession = new R2dbcSession(delegate, UUID.randomUUID().toString(), true);
            return Mono.just(r2dbcSession).doOnNext(session -> publishEvent(new SessionCreatedEvent(this, session)));
        });
    }

    @Override
    public Mono<Void> save(final R2dbcSession session) {
        if (session.isNew()) {
            return databaseClient.execute(createSessionQuery)
                    .bind(0, session.originalSessionId)
                    .bind(1, session.getId())
                    .bind(2, session.getCreationTime().toEpochMilli())
                    .bind(3, session.getLastAccessedTime().toEpochMilli())
                    .bind(4, (int) session.getMaxInactiveInterval().getSeconds())
                    .bind(5, session.getExpiryTime().toEpochMilli())
                    .fetch().rowsUpdated()
                    .then(saveSessionAttributes(session, new ArrayList<>(session.getAttributeNames())))
                    .as(transactionalOperator::transactional)
                    .doOnSuccess((aVoid) -> session.clearChangeFlags());
        } else {
            List<String> addedAttributeNames = session.delta.entrySet().stream()
                    .filter((entry) -> entry.getValue() == DeltaValue.ADDED).map(Map.Entry::getKey)
                    .collect(Collectors.toList());

            List<String> updatedAttributeNames = session.delta.entrySet().stream()
                    .filter((entry) -> entry.getValue() == DeltaValue.UPDATED).map(Map.Entry::getKey)
                    .collect(Collectors.toList());

            List<String> removedAttributeNames = session.delta.entrySet().stream()
                    .filter((entry) -> entry.getValue() == DeltaValue.REMOVED).map(Map.Entry::getKey)
                    .collect(Collectors.toList());

            return databaseClient.execute(this.updateSessionQuery)
                    .bind(0, session.getId())
                    .bind(1, session.getCreationTime().toEpochMilli())
                    .bind(2, (int) session.getMaxInactiveInterval().getSeconds())
                    .bind(3, session.getExpiryTime().toEpochMilli())
                    .bind(4, session.originalSessionId)
                    .fetch().rowsUpdated()
                    .then(saveSessionAttributes(session, addedAttributeNames))
                    .then(updateSessionAttributes(session, updatedAttributeNames))
                    .then(deleteSessionAttributes(session, removedAttributeNames))
                    .as(transactionalOperator::transactional)
                    .doOnSuccess((aVoid) -> session.clearChangeFlags());
        }
    }

    @Override
    public Mono<R2dbcSession> findById(final String id) {
        return databaseClient.execute(getSessionQuery).bind(0, id)
                .fetch()
                .all()
                .collectList()
                .map(new R2dbcSessionConverter())
                .filter(sessions -> !sessions.isEmpty())
                .map(sessions -> sessions.get(0))
                .filter(session -> !session.isExpired())
                .switchIfEmpty(Mono.defer(() -> deleteById(id).then(Mono.empty())));
    }

    @Override
    public Mono<Void> deleteById(final String id) {
        return databaseClient.execute(deleteSessionQuery).bind(0, id).then().as(transactionalOperator::transactional);
    }

    public Mono<Void> saveSessionAttributes(R2dbcSession session, List<String> attributeNames) {
        Assert.notNull(attributeNames, "attributeNames must not be null or empty");
        if (attributeNames.isEmpty()) {
            return Mono.empty();
        }
        if (attributeNames.size() > 1) {
            return Flux.fromIterable(attributeNames).concatMap((attr) -> saveSessionAttribute(session, attr)).then();
        }
        return saveSessionAttribute(session, attributeNames.get(0));
    }

    public Mono<Void> saveSessionAttribute(R2dbcSession session, String attributeName) {
        return this.databaseClient.execute(createSessionAttributeQuery)
                .bind(0, attributeName)
                .bind(1, serialize(session.getAttribute(attributeName)))
                .bind(2, session.getId())
                .then().as(transactionalOperator::transactional);
    }

    public Mono<Void> updateSessionAttributes(R2dbcSession session, List<String> attributeNames) {
        Assert.notNull(attributeNames, "attributeNames must not be null or empty");
        if (attributeNames.isEmpty()) {
            return Mono.empty();
        }
        if (attributeNames.size() > 1) {
            return Flux.fromIterable(attributeNames).concatMap((attr) -> updateSessionAttribute(session, attr)).then();
        }
        return updateSessionAttribute(session, attributeNames.get(0));
    }

    public Mono<Void> updateSessionAttribute(R2dbcSession session, String attributeName) {
        return databaseClient.execute(updateSessionAttributeQuery)
                .bind(0, serialize(session.getAttribute(attributeName)))
                .bind(1, session.originalSessionId)
                .bind(2, attributeName)
                .then().as(transactionalOperator::transactional);
    }

    public Mono<Void> deleteSessionAttributes(R2dbcSession session, List<String> attributeNames) {
        Assert.notNull(attributeNames, "attributeNames must not be null or empty");
        if (attributeNames.isEmpty()) {
            return Mono.empty();
        }
        if (attributeNames.size() > 1) {
            return Flux.fromIterable(attributeNames).concatMap((attr) -> deleteAttribute(session, attr)).then();
        }
        return deleteAttribute(session, attributeNames.get(0));
    }

    public Mono<Void> deleteAttribute(R2dbcSession session, String attributeName) {
        return databaseClient.execute(deleteSessionAttributeQuery)
                .bind(0, session.originalSessionId)
                .bind(1, attributeName)
                .then().as(transactionalOperator::transactional);
    }

    public Mono<Void> clearExpiredSessions() {
        log.info("Clean expired sessions");
        return databaseClient.execute(deleteSessionsByExpiryTimeQuery)
                .bind(0, System.currentTimeMillis())
                .then().as(transactionalOperator::transactional);
    }

    private byte[] serialize(Object object) {
        return (byte[]) this.conversionService.convert(object, TypeDescriptor.valueOf(Object.class), TypeDescriptor.valueOf(byte[].class));
    }

    private Object deserialize(byte[] bytes) {
        return this.conversionService.convert(bytes, TypeDescriptor.valueOf(byte[].class), TypeDescriptor.valueOf(Object.class));
    }

    private static GenericConversionService createDefaultConversionService() {
        GenericConversionService converter = new GenericConversionService();
        converter.addConverter(Object.class, byte[].class, new SerializingConverter());
        converter.addConverter(byte[].class, Object.class, new DeserializingConverter());
        return converter;
    }

    private void publishEvent(ApplicationEvent event) {
        if (eventPublisher != null) {
            try {
                eventPublisher.publishEvent(event);
            } catch (Throwable ex) {
                log.error("Error publishing " + event + ".", ex);
            }
        }
    }

    private String getQuery(String base) {
        return StringUtils.replace(base, "%TABLE_NAME%", this.tableName);
    }

    private void prepareQueries() {
        this.createSessionQuery = getQuery(CREATE_SESSION_QUERY);
        this.createSessionAttributeQuery = getQuery(CREATE_SESSION_ATTRIBUTE_QUERY);
        this.getSessionQuery = getQuery(GET_SESSION_QUERY);
        this.updateSessionQuery = getQuery(UPDATE_SESSION_QUERY);
        this.updateSessionAttributeQuery = getQuery(UPDATE_SESSION_ATTRIBUTE_QUERY);
        this.deleteSessionAttributeQuery = getQuery(DELETE_SESSION_ATTRIBUTE_QUERY);
        this.deleteSessionQuery = getQuery(DELETE_SESSION_QUERY);
        this.deleteSessionsByExpiryTimeQuery = getQuery(DELETE_SESSIONS_BY_EXPIRY_TIME_QUERY);
    }

    private enum DeltaValue {

        ADDED, UPDATED, REMOVED

    }

    public final class R2dbcSession implements Session {

        private final Session delegate;
        
        private final String originalSessionId;

        private boolean isNew;

        private boolean changed;

        private final Map<String, DeltaValue> delta = new HashMap<>();

        R2dbcSession(MapSession delegate, String primaryKey, boolean isNew) {
            this.delegate = delegate;
            this.originalSessionId = primaryKey;
            this.isNew = isNew;
            if (this.isNew || (R2dbcSessionRepository.this.saveMode == SaveMode.ALWAYS)) {
                getAttributeNames().forEach((attributeName) -> this.delta.put(attributeName, DeltaValue.UPDATED));
            }
        }

        public boolean isNew() {
            return this.isNew;
        }

        public boolean isChanged() {
            return this.changed;
        }

        public Instant getExpiryTime() {
            return getLastAccessedTime().plus(getMaxInactiveInterval());
        }

        public Map<String, DeltaValue> getDelta() {
            return this.delta;
        }

        public void clearChangeFlags() {
            this.isNew = false;
            this.changed = false;
            this.delta.clear();
        }

        @Override
        public String getId() {
            return this.delegate.getId();
        }

        @Override
        public String changeSessionId() {
            this.changed = true;
            return this.delegate.changeSessionId();
        }

        @Override
        public <T> T getAttribute(String attributeName) {
            Supplier<T> supplier = this.delegate.getAttribute(attributeName);
            if (supplier == null) {
                return null;
            }
            T attributeValue = supplier.get();
            if (attributeValue != null && R2dbcSessionRepository.this.saveMode.equals(SaveMode.ON_GET_ATTRIBUTE)) {
                this.delta.put(attributeName, DeltaValue.UPDATED);
            }
            return attributeValue;
        }

        @Override
        public Set<String> getAttributeNames() {
            return this.delegate.getAttributeNames();
        }

        @Override
        public void setAttribute(String attributeName, Object attributeValue) {
            boolean attributeExists = this.delegate.getAttribute(attributeName) != null;
            boolean attributeRemoved = (attributeValue == null);
            if (!attributeExists && attributeRemoved) {
                return;
            }
            if (attributeExists) {
                if (attributeRemoved) {
                    this.delta.merge(attributeName,  DeltaValue.REMOVED, (oldDeltaValue, deltaValue) -> (oldDeltaValue ==  DeltaValue.ADDED) ? null : deltaValue);
                } else {
                    this.delta.merge(attributeName, DeltaValue.UPDATED, (oldDeltaValue, deltaValue) -> (oldDeltaValue == DeltaValue.ADDED) ? oldDeltaValue : deltaValue);
                }
            } else {
                this.delta.merge(attributeName, DeltaValue.ADDED, (oldDeltaValue, deltaValue) -> (oldDeltaValue == DeltaValue.ADDED) ? oldDeltaValue : DeltaValue.UPDATED);
            }
            this.delegate.setAttribute(attributeName, value(attributeValue));
        }

        @Override
        public void removeAttribute(String attributeName) {
            setAttribute(attributeName, null);
        }

        @Override
        public Instant getCreationTime() {
            return this.delegate.getCreationTime();
        }

        @Override
        public void setLastAccessedTime(Instant instant) {
            this.delegate.setLastAccessedTime(instant);
            this.changed = true;
        }

        @Override
        public Instant getLastAccessedTime() {
            return this.delegate.getLastAccessedTime();
        }

        @Override
        public void setMaxInactiveInterval(Duration duration) {
            this.delegate.setMaxInactiveInterval(duration);
            this.changed = true;
        }

        @Override
        public Duration getMaxInactiveInterval() {
            return this.delegate.getMaxInactiveInterval();
        }

        @Override
        public boolean isExpired() {
            return this.delegate.isExpired();
        }
    }

    public final class R2dbcSessionConverter implements Function<List<Map<String, Object>>, List<R2dbcSession>> {

        @Override
        public List<R2dbcSession> apply(List<Map<String, Object>> rows) {
            List<R2dbcSession> sessions = new ArrayList<>();
            for (Map<String, Object> row : rows)  {
                String id = (String) row.get("SESSION_ID");
                R2dbcSession session;
                if (sessions.size() > 0 && getLast(sessions).getId().equals(id)) {
                    session = getLast(sessions);
                } else {
                    MapSession delegate = new MapSession(id);
                    String primaryKey = (String) row.get("PRIMARY_ID");
                    Long creationTime = (Long) row.get("CREATION_TIME");
                    if (creationTime == null) {
                        handleMissingKey("CREATION_TIME");
                    }
                    delegate.setCreationTime(Instant.ofEpochMilli(creationTime));
                    Long lastAccessedTime = (Long) row.get("LAST_ACCESS_TIME");
                    if (lastAccessedTime == null) {
                        handleMissingKey("LAST_ACCESS_TIME");
                    }
                    delegate.setLastAccessedTime(Instant.ofEpochMilli(lastAccessedTime));
                    Integer maxInactiveInterval = (Integer) row.get("MAX_INACTIVE_INTERVAL");
                    if (maxInactiveInterval == null) {
                        handleMissingKey("MAX_INACTIVE_INTERVAL");
                    }
                    delegate.setMaxInactiveInterval(Duration.ofSeconds(maxInactiveInterval));
                    session = new R2dbcSession(delegate, primaryKey, false);
                }
                String attributeName = (String) row.get("ATTRIBUTE_NAME");
                if (attributeName != null) {
                    if (row.get("ATTRIBUTE_BYTES") instanceof ByteBuffer) {
                        ByteBuffer byteBuffer = (ByteBuffer) row.get("ATTRIBUTE_BYTES");
                        byte[] bytes = byteBuffer.array();
                        session.delegate.setAttribute(attributeName, lazily(() -> deserialize(bytes)));
                    } else if (row.get("ATTRIBUTE_BYTES") instanceof byte[]){
                        byte[] bytes = (byte[]) row.get("ATTRIBUTE_BYTES");
                        session.delegate.setAttribute(attributeName, lazily(() -> deserialize(bytes)));
                    } else if (row.get("ATTRIBUTE_BYTES") instanceof Blob) {
                        ByteBuffer buffer = Mono.from(((Blob) row.get("ATTRIBUTE_BYTES")).stream()).toFuture().join();
                        byte[] bytes = new byte[buffer.remaining()];
                        buffer.rewind();
                        buffer.get(bytes);
                        session.delegate.setAttribute(attributeName, lazily(() -> deserialize(bytes)));
                    }
                }
                sessions.add(session);
            }
            return sessions;
        }

        private void handleMissingKey(String key) {
            throw new IllegalStateException(key + " key must not be null");
        }

        private R2dbcSession getLast(List<R2dbcSession> sessions) {
            return sessions.get(sessions.size() - 1);
        }
    }

} 
