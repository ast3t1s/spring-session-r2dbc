# Spring Session R2bc
[![MIT License](https://img.shields.io/badge/license-MIT-brightgreen.svg)](https://opensource.org/licenses/MIT)
[![pipeline status](https://gitlab.com/ast3t1s/spring-session-r2dbc/badges/master/pipeline.svg)](https://gitlab.com/ast3t1s/spring-session-r2dbc/-/commits/master)
[![coverage report](https://gitlab.com/ast3t1s/spring-session-r2dbc/badges/master/coverage.svg)](https://gitlab.com/ast3t1s/spring-session-r2dbc/-/commits/master)
[![last-commit](https://gitlab.com/ast3t1s/spring-session-r2dbc/-/jobs/artifacts/master/raw/last-commit.svg?job=badge)](https://gitlab.com/ast3t1s/spring-session-r2dbc/-/commits/master)
[![release-version](https://img.shields.io/badge/release-1.0.0-orange.svg?style=flat-square&logo=appveyor)](https://gitlab.com/ast3t1s/spring-session-r2dbc/-/commits/release)
[![snapshot-version](https://img.shields.io/badge/snapshot-1.0.0-brightgreen.svg?style=flat-square&logo=appveyor)](https://gitlab.com/ast3t1s/spring-session-r2dbc/-/commits/master)

Spring Session [R2dbc](https://github.com/r2dbc)  is community project provides ReactiveSessionRepository implementation backed by relational 
databases using a reactive driver.

## Features
* WebSession - allows replacing the Spring WebFlux’s WebSession in an application container neutral way

## Getting Started

Here is a quick teaser of an application using Spring Session R2dbc for more detail see docs:
```java
@EnableR2dbcWebSession
@SpringBootApplication
public class SpringSessionR2dbcApplication {
    
    public static void main(String[] args) {
		SpringApplication.run(SpringSessionsR2dbcApplication.class, args);
    }
}

@Configuration
public class R2dbcConnectionConfiguration extends AbstractR2dbcConfiguration {
    
        @Bean
        @Override
        public ConnectionFactory connectionFactory() {
            return new H2ConnectionFactory(H2ConnectionConfiguration.builder()
                    .inMemory("r2dbc")
                    .username("sa")
                    .password("")
                    .option("DB_CLOSE_DELAY=-1").build());
        }
    
        @Bean
        public ResourceDatabasePopulator databasePopulator() {
            return new ResourceDatabasePopulator(new ClassPathResource("com/spring/session/r2dbc/schema-h2.sql"));
        }
    
        @Bean
        public ConnectionFactoryInitializer initializer(ConnectionFactory connectionFactory, ResourceDatabasePopulator databasePopulator) {
            ConnectionFactoryInitializer initializer = new ConnectionFactoryInitializer();
            initializer.setConnectionFactory(connectionFactory);
    
            CompositeDatabasePopulator populator = new CompositeDatabasePopulator();
            populator.addPopulators(databasePopulator);
            initializer.setDatabasePopulator(populator);
    
            return initializer;
        }
    
        @Bean
        public ReactiveTransactionManager transactionManager(ConnectionFactory connectionFactory) {
            return new R2dbcTransactionManager(connectionFactory);
        }
}
```

### Maven configuration
```xml
<dependency>
  <groupId>com.astetis</groupId>
  <artifactId>spring-session-r2dbc</artifactId>
  <version>${version}</version>
</dependency>
```
#### Snapshot version
```xml
<dependency>
  <groupId>com.astetis</groupId>
  <artifactId>spring-session-r2dbc</artifactId>
  <version>${version}-SNAPSHOT</version>
</dependency>
```

## Building from Source
You don't need to build from source to use Spring Session R2dbc, but if you want to try out, Spring Session R2dbc cane be
easily build with the maven wrapper. You also need JDK >= 1.8.
```shell script
    $ ./mvnw clean install
```
If you want to build with the regular mvn you will need Maven

## License

Copyright 2020.

Licensed under the MIT License.
